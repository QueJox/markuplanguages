= Llenguatges de marques lleugers
:author: Josep Queralt
:email: josep.queralt@copernic.cat
:doctype: book
:encoding: utf-8
:lang: ca

== Transparències

* link:slides/slides.html[slides + reveal]
* link:slides/slides_static.html[slides + estàtiques]
* link:slides/slides.pdf[slides + pdf]

== Demo

* Demo link:demo3/demo.html[(html)]
* Demo versió AsciidoctorFX link:demo3/demov1.pdf[(pdf)]
* Demo versió Asciidocor link:demo3/demov2.pdf[(pdf)]

== Exemples Backends

Un únic document compilat amb diferents backends i fulls d'estil.

* link:demo2/M03ProgramacioPart1v1.pdf[Versió 1 en pdf]
* link:demo2/M03ProgramacioPart1v2.pdf[Versió 2 en pdf]
* link:demo2/M03ProgramacioPart1v1.html[Versió 1 en html]
* link:demo2/M03ProgramacioPart1v2.html[Versió 2 en html]

