= Manual d'estil

<<<

=== Avís important

[IMPORTANT]
====
No seguir el manual d'estil comportarà una baixada de la nota tant de les pràctiques com dels exàmens fins al punt de considerar-les no entregades i per tant avaluades amb un zero.
====

<<<

=== Variables

==== Notació "Camel-case"

Totes les variables, *no constants*, en Java s'escriuen en notació _camel-case_ això és:

* *Sempre* comencen en minúscula.
* Si el nom de la variable és un nom compost, la primera lletra de cada paraula, excepte la primera, s'escriu en majúscula.
* Les demés lletres s'escriuen totes en minúscula.
* No s'utilitza el guió baix per separar les diferents parts del nom d'una variable.

.Correcte
[source, java]
----
metodePagament
comptador
diaDelMes
fitxerSortida
----

.Incorrecte
----
MetodePagament
COMPTADOR
diadelmes
fitxer_sortida
----

[WARNING]
====
Aquestes convencions fan referència al llenguatge Java, altres llenguatges utilitzen diferents convencions que poden no tenir res a veure amb aquestes.
====

==== Constants

Si la variable es tracta com una constant s'escriu en majúscula i si es tracta de noms compostos es separa cada part del nom amb un guió baix.

.Correcte
[source, java]
----
MAX_CONNEXIONS
PI
----

==== Números màgics

No utilitzar números màgics. Un número màgic és una constant numèrica dins del codi font.

Fins i tot la constant còsmica més raonable canviarà algun dia. Penseu que un any té 365 dies? Els vostres compradors marcians no estaran massa contents amb els vostres prejudicis.

.Incorrecte
[source, java]
----
if (p.getX() < 300)
----

.Correcte
[source, java]
----
final double AMPLADA_FINESTRA = 300;
. . . 
if (p.getX() < AMPLADA_FINESTRA) 
----

==== Utilitzar noms que mostrin les intencions

Si un nom de variable necessita un comentari, vol dir que no indica la seva funció.

.Incorrecte
[source, java]
----
int d; // Temps en transcorregut en dies
int comptador; // Conta el nombre de paraules de la cadena
----

.Correcte
[source, java]
----
int diesDesDeLaCreacio;
int nombreDeParaules;
----

[NOTE]
====
Algunes variables sense significat semàntic no necessiten cap comentari perquè queda clar quin és el seu paper, en aquest cas es poden fer servir.

Per exemple:

[source, java]
----
for (int i=0, i<10;i++) {
     . . . 
}
----
====

[TIP]
====
També és útil tenir en compte que els noms de les variables:

* Es puguin pronunciar.
* Es puguin buscar.
====

==== Evitar caràcters no americans als noms de les variables

No sabem ni la codificació del fitxer amb el codi font ni la regió del món on es pot examinar.

Dins el codi no hauria d'aparèixer cap caràcter del primer bloc (127 primers caràcters) de la codificació ASCII.

Això vol dir que no han d'aparèixer, ni accents, ni la c trencada ni símbols potencialment perillosos com el símbol del euro.

La única excepció a això és a dins d'un *comentari* o a dins d'una *cadena*.

=== Format

El format del codi és important!!

El format del codi font té a veure amb la comunicació, i la comunicació és és la principal habilitat en un projecte de desenvolupament de software.

El "*fer que funcioni*" és secundari si el codi font no s'entén o no és fàcilment llegible.

Hi ha moltes possibilitats que la funcionalitat que creeu avui canvii en la pròxima versió, però la llegibilitat del codi tindrà un efecte profund en tots els canvis que es realitzaran d'ara fins al final de la vida del producte. El vostre estil i disciplina sobreviurà encara que el vostre codi font no ho faci.

==== Sangrat

* El contingut d'un bloc de codi sempre es sagna cap a la dreta respecte la línia que obre el bloc.
* El codi es torna a sagnar cap a l'esquerra a la línia en que el bloc finalitza. 
* El sagnat cap a la dreta i cap a l'esquerra ha de ser exactament de la mateixa mida.
* La línia que obre un bloc i la línia que el tanca han d'estar alineades horitzontalment.
* Aquest sagnat pot ser de dos, tres o quatre espais però cal ser consistent, tot el codi ha de mantenir el mateix sagnat.

[NOTE]
====
Recordeu que no cal posar claus en un bloc d'una sola línia.
====

[WARNING]
====
És preferible fer servir espais que tabulacions, la codificació del codi tabulador pot donar problemes en alguns editors.

La majoria d'editors de text, però, permeten configurar que la tecla de tabulació no imprimeixi el caràcter de tabulació sinó un nombre determinat d'espais.
====

.Incorrecte
[source, java]
----
for(int i=0; i<10; i++)
System.out.println(i);
----

.Correcte
[source, java]
----
for(int i=0; i<10; i++)
    System.out.println(i);
----

.Incorrecte
[source, java]
----
while (a < 10) {
if(a % 2 == 0) {
System.out.println(a + " és parell");
}
}
----

.Incorrecte
[source, java]
----
while (a < 10) {
  if(a % 2 == 0) {
       System.out.println(a + " és parell");
 }
}
----

.Correcte
[source, java]
----
while (a < 10) {
    if(a % 2 == 0) {
        System.out.println(a + " és parell");
    }
}
----

==== Format vertical

El codi ha de ser fàcil de llegir verticalment, de dalt cap a baix.

* Separar els grups de línies conceptualment relacionades amb una línia en blanc.
+
.Incorrecte
[source, java]
----
package fitnesse.wikitext.widgets;
import java.util.regex.*;
public class BoldWidget extends ParentWidget {
    public static final String REGEXP = "'''.+?'''";
    private static final Pattern pattern = Pattern.compile("'''(.+?)'''", Pattern.MULTILINE + Pattern.DOTALL);
    public BoldWidget(ParentWidget parent, String text) throws Exception {
        super(parent);
        Matcher match = pattern.matcher(text);
        match.find();
        addChildWidgets(match.group(1));
    }
    public String render() throws Exception {
        StringBuffer html = new StringBuffer("<b>");
        html.append(childHtml()).append("</b>");
        return html.toString();
    }
}
----
+
.Correcte
[source, java]
----
package fitnesse.wikitext.widgets;

import java.util.regex.*;

public class BoldWidget extends ParentWidget {
    public static final String REGEXP = "'''.+?'''";
    private static final Pattern pattern = Pattern.compile("'''(.+?)'''", Pattern.MULTILINE + Pattern.DOTALL);
    
    public BoldWidget(ParentWidget parent, String text) throws Exception {
        super(parent);
        Matcher match = pattern.matcher(text);
        match.find();
        addChildWidgets(match.group(1));
    }
    
    public String render() throws Exception {
        StringBuffer html = new StringBuffer("<b>");
        html.append(childHtml()).append("</b>");
        return html.toString();
    }
}
----
. No separar els grups de línies conceptualment relacionades amb línies en blanc.
+
.Incorrecte
[source, java]
----
public class ReporterConfig {

    /**
    * The class name of the reporter listener
    */
    private String m_className;
    
    /**
    * The properties of the reporter listener
    */
    private List<Property> m_properties = new ArrayList<Property>();
    
    public void addProperty(Property property) {
        m_properties.add(property);
    }
}
----
+
.Correcte
[source, java]
----
public class ReporterConfig {

    private String m_className;
    private List<Property> m_properties = new ArrayList<Property>();
    
    public void addProperty(Property property) {
        m_properties.add(property);
    }
}
---- 

==== Control de flux

==== Bucles for

Utilitzar un bucle for només en el cas que una variable va d'un valor a un altre valor amb un *increment constant*.

.Correcte
[source, java]
----
for (int i = 0; i < a.length; i++) { 
    System.out.println(a[i]); 
}
----

No utilitzar un bucle _for_ per construccions estranyes com ara:

.Incorrecte
[source, java]
----
for (a = a / 2; count < ITERATIONS; System.out.println(xnew));
----

Un bucle com l'anterior és un bucle while.

.Correcte
[source, java]
----
a = a / 2; 
while (count < ITERATIONS) // OK 
{ 
    . . . 
    System.out.println(xnew); 
}
----

==== Ús de break i continue

Evitar al *màxim* l'ús de les instruccions _break_ i _continue_. 

Millor utilitzar variables de control per gestionar el flux d'execució.

[NOTE]
====
Si que està permès utilitzar la sentència _break_ en una estructura _switch_.
====

==== Ús de System.exit()

No utilitzar _System.exit()_ per finalitzar el flux d'execució d'un programa.

=== Funcions i mètodes

==== Han de tenir un mida reduïda

Unes 20 línies màxim.

Això implica que les instruccions _if_, _else_, _while_ i similars probablement tindran una línia de longitud, una crida a una funció.

==== Han de fer una sola cosa

Les funcions només han de fer una cosa, fer-la bé i ha de ser la única cosa que facin.

.Incorrecte
[source, java]
----
int[] invertirVector(int[] vector) {
    int[] vectorInvertit = new int[vector.length];
    for(int i = 0; i < vector.length; i++) {
        vectorInvertit[i] = vector[vector.length - 1 - i]
        System.out.print(vectorInvertit[i] + " ");
    }
    return vectorInvertit;
}
----

.Correcte
[source, java]
----
int[] invertirVector(int[] vector) {
    int[] vectorInvertit = new int[vector.length];
    for(int i = 0; i < vector.length; i++) {
        vectorInvertit[i] = vector[vector.length - 1 - i]
    }
    return vectorInvertit;
}

void mostrarVector(int[] vector) {
    for(int i = 0; i < vector.length; i++) {
        System.out.print(vector[i] + " ");
    } 
}
----

==== Han de tenir noms descriptius 

((TODO))

==== Han de tenir pocs paràmetres d'entrada

TODO

==== El nom dels paràmetres ha de ser explicit

El nom dels paràmetres ha de ser explícit, especialment si són _integer_ o _boolean_.

.Incorrecte
[source, java]
----
public void afegirAlumne(String s1, String s2, String s3) 
----

.Correcte
[source, java]
----
public void afegirAlumne(String nom, String cognoms, String dni) 
----

=== Comentaris

==== Els comentaris no compensen el codi incorrecte

Una de les motivacions més comuns per escriure comentaris és el codi mal escrit.

Escribim un mòdul de codi confús i desorganitzat, *sabem que és confús i desorganitzat* i per això decidim comentar-lo.

*El que hem de fer és reescriure'l de manera que deixi de ser confús i desorganitzat*.

==== És millor que el codi sigui autoexplicatiu

Un codi autoexplicatiu evita la necessita de posar comentaris:

[source, java]
----
// Comprovem si el treballador és candidat a una bonificació
if ((treballador.edat > 65) && (treballador.horesSetmanalsTreballades >= 40))
----

És millor:

[source, java]
----
if(treballadors.esCandidatABonificacio())
----

==== Bons comentaris

* Comentaris legals
* Comentaris informatius
** Resumeixen la intenció d'un mòdul o classe de manera que permeten entendre el seu objectiu amb un cop d'ull.
* Avisar de les consequències de canviar o executar un determinat bloc de codi.
* Resaltar la importància d'alguna cosa que pot semblar insubstancial.
* Documentar les APIs públiques.

==== Mals comentaris

* Escriure un comentari per que toca sense invertir el temps necessari en assegurar-se que és el millor comentari que espot escriure.
* Descriure literalment el que està fent el codi.
+
.Incorrecte
[source, java]
----
// Métode d'utilitat que retorna quan this.closed és true. Llença una excepció i s'esgota el temps d'espera.
public synchronized void waitForClose(final long timeoutMillis) throws Exception
{
    if(!closed) {
        wait(timeoutMillis);
        if(!closed) {
            throw new Exception("No es pot tancar el MockResponseSender");
        }
    }
}
----
* Comentaris ambigus que no descriuen precisa o acuradament el que volen descriure.
+
El comentari de l'exemple anterior: 
+
[source, java]
----
Mètode d'utilitat que retorna quan this.closed és true
----
+
És incorrecte, el mètode retorna *si* this.closed és true, no *quan* this.closed és true.

=== Classes 

==== Notació 

Els noms de les classes en Java segueixen les següents convencions:

* *Sempre* comencen en majúscula.
* Si el nom de la Classe és un nom compost, la primera lletra de cada paraula, incloent-hi la primera, s'escriu en majúscula.
* Les demés lletres s'escriuen totes en minúscula.
* No s'utilitza el guió baix per separar les diferents parts del nom d'una classe.

=== Excepcions

==== Excepcions genèriques

No capturar excepcions genèriques.

.Incorrecte
[source, java]
----
Widget readWidget(Reader in) throws Exception 
----

Declarar o capturar les excepcions explícites que pot llançar el el bloc de codi o el mètode

.Correcte
[source, java]
----
Widget readWidget(Reader in) throws IOException, MalformedWidgetException 
----

==== No amagar les excepcions

No amagar les excepcions simplement perquè el compilador no es queixi.

.Incorrecte
[source, java]
----
try { 
    double preu = in.readDouble(); 
} catch (Exception e) {

} 
----

.Correcte
[source, java]
----
try { 
    double preu = in.readDouble(); 
} catch (Exception e) {
    e.printStackTrace();
} 
----
