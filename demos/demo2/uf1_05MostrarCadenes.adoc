== Mostrar literals, nombres i caràcters

=== Mostrar literals

Considerem el següent fragment de codi:

[source,java]
----
System.out.println("Hello, World!");
----

* Les frases que apareixen entre *cometes dobles* s'anomenen *literals*, *string literals* en anglès.
* Per mostrar un literal per pantalla s'utilitzen dues comandes:
((System.out.println()));::
Mostra la cadena passada i afegeix un salt de línia al final.
((System.out.print()));::
Mostra la cadena passada sense afegir un salt de línia.

Per exemple:

[source,java]
----
public class Adeu {
    public static void main(String[] args) {
        System.out.print("Adéu, ");
        System.out.println("món cruel");
        System.out.println("#####");
    }
}
----

Mostraria:

----
Adéu món cruel
#####
----

=== ((Seqüències d'escapament))

En una cadena poden aparèixer caràcters que difícilment es poden introduir des del teclat, o bé perquè no tenen una representació gràfica o bé per que trenquen alguna d eles regles de la construcció de les cadenes.

Per exemple:

* Posar una cometa doble dins d'una cadena.
* Posar un tabulador dins d'una cadena.
* Posar un caràcter que no està representat al teclat però si existeix a la codificació de caràcters utilitzada, normalment UTF-8 en Java, *&#xb1;* o *&#654;* per exemple.

Un caràcter també es pot representar com una seqüència d'escapament.

* Una seqüència d'escapament comença per un */* i a continuació un caràcter o un codi que fa referència al caràcter que es vol imprimir.

.Seqüències de caràcter d'escapament
\n:: A linefeed
\r:: A carriage return
\b:: Backspace
\t:: Tabulador
\\:: Contrabarra
\":: Cometa doble
\':: Cometa simple

També és possible definir un literal de caràcter a partir d'una seqüència d'escapament unicode.

\u0041::
A
\u0042::
B

etc...

.Final de línia
****
En els sistemes unix, tradicionalment, la finalització d'una línia es denota amb el caràcter \n (new-line).

En Windows, tradicionalment, el salt de línia es denota amb els caràcters /r/n (carriage-return i new-line) per herència dels antics terminals de tipus teletip.

Actualment la majoria de programes accepten el caràcter \n com a final de línia, en particular Java.
****

.Codificació de caràcters
****
Per aconseguir l'intercanvi d'informació entre dos sistemes cal que un dels sistemes sigui capaç d'entendre de manera no ambigua la codificació dels caràcters representats, en una combinació de bits, i generats per l'altre sistema, i viceversa.

La codificació de caràcters en els sistemes informàtics es basa en els següents fets:

* Cada caràcter es s'associa a un enter no negatiu anomenat "code point".
* Un "character set" és un conjunt de caràcters junt amb els "code points" corresponents.
* El nombre de bits utilitzats per a representar un caràcter determina la quantitat de caràcters diferents que es poden representar en un "charset" determinat


Alguns dels charsets més utilitzats són:

ASCII::
* Codifica cada caràcter amb 7 bits, cosa que proporciona 128 caràcters diferents per charset.
ASCII estès::
* Codifica cada caràcter amb 8 bits.
****

****
UCS::
* Utilitza una estructura de 32 bits per a representar un caràcter i pretén proporcionar un únic charset per a tots els llenguatges actuals i passats. 
* També proporciona símbols utilitzats en diferents disciplines com  ara matemàtiques i física.
* UCS-2, UCS-4, UTF-16, UTF-8 són diferents mètodes de codificació de caràcters dins de UCS.
unicode footnote:[http://www.unicode.org]::
* Es pot considerar un subconjunt de UCS i codifica cada caràcter amb 16 bits.
* *Els primers 128 caràcters d'unicode són els mateixos que en ASCII*, cosa que facilita la conversió entre els dos sistemes.
* Alguns caràcters poc comuns utilitzen dos "code points" per a ser representats.
****

=== Mostrar caràcters

Java distingeix entre un *literal cadena* i un *caràcter*, en el primer cas es tracta d'un tipus de dades de longitud no definida, en el segon es tracta d'un valor numèric d'exactament dos bytes. Per aquesta raó el tractament intern que es d'un tipus de dades i de l'altre es diferent, la gestió de la memòria no és la mateixa ja que en un cas la longitud és sempre la mateixa i en l'altre no.

Els caràcters es delimiten per *cometes simples* a diferència de les cadenes que es delimiten per cometes dobles.

Les comandes utilitzades per mostrar caràcters són les mateixes que per mostrar literals de tipus cadena:

* System.out.println('_caràcter_');
* System.out.print('_caràcter_');

Per exemple:

[source, java]
----
System.out.println('a');
System.out.println('\u0050');
System.out.print('\n');
----

[NOTE]
====
Un caràcter es pot tractar com a caràcter o com a cadena de longitud 1 depenent de si es delimita amb cometes simples o dobles.
====

=== Mostrar nombres

Per mostrar un nombre en Java s'utilitzen les mateixes comandes que per mostrar literals de tipus cadena, en el  cas dels nombres però *no es delimiten amb cometes*:

* System.out.println(_nombre_);
* System.out.print(_nombre_);

Els nombres en Java poden ser enters o en coma flotant, també es poden representar en notació científica footnote:[Veure l'apartat dedicat a tipus de dades].

[IMPORTANT]
====
El separador decimal d'un nombre decimal és el punt, no la coma.
====

[NOTE]
====
Un nombre es pot tractar com una cadena si es delimita amb cometes dobles o com a un caràcter si té un sol dígit i es delimita entre cometes simples. 

No obstant el tractament que es pot fer d'un nombre, d'una cadena o d'un caràcter no és el mateix, per exemple, els nombres es poden operar aritmèticament, les cadenes i els caràcters no.
====

Per exemple:

[source, java, subs="quotes"]
----
System.out.println(4);
System.out.println(4.5);
System.out.println(3E5); // 3 x 10^5
System.out.println("49"); // Es tracta el nombre 49 com una cadena
System.out.println(4 * 6); // Correcte 4 x 6 = 24
System.out.println('4' * '6'); // Error, no té sentit multiplicar caràcters
----